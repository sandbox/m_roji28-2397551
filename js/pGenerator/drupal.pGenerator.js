(function ($) {
/**
 * Attaches the autocomplete behavior to all required fields.
 */
Drupal.behaviors.pGenerator = {
  attach: function (context, settings) {  
    if (settings.pGenerator !== undefined) {
      var item = settings.pGenerator;
      for (index in item) {
        var each = item[index];
        $(each.feeder).once('pGenerator', function() {
          console.log(each);
          var conf = {
            'bind': 'click',
            'passwordElement': each.target, //
            // 'displayElement': '#my-display-element',
            'passwordLength': 32,
            'uppercase': true,
            'lowercase': true,
            'numbers':   true,
            'specialChars': false,
            // 'onPasswordGenerated': function(generatedPassword) {
              // alert('My new generated password is ' + generatedPassword);
            // }
          }
          $(each.feeder).pGenerator(conf);
        });
      }
    }
  },
};
})(jQuery);
