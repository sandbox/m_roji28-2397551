(function ($, undefined) {

  // Enable Timago only if a user's browser has localStorage support.
  // They must be able to double click it to enable/disable at will.
  var localStorage = 'localStorage' in window && typeof window.localStorage !== 'undefined' && window['localStorage'] !== null;
  var timeagoEnabled = localStorage ? (typeof window.localStorage['drupalorgCommentTimeago'] !== 'undefined' && window.localStorage['drupalorgCommentTimeago'] === 'false' ? false : true) : false;

  // Callback for toggling timestamps between set dates and relative time.
  function toggleTimes () {
    var $element = $(this);

    // Retrieve the original created date.
    var createdDate = $element.attr('data-created-date');
    if (!createdDate) {
      createdDate = $element.text();
      $element.attr('data-created-date', createdDate);
    }

    // Determine which title to use.
    var title = timeagoEnabled ? Drupal.t('!created_date (double click to toggle)', {
      '!created_date': createdDate
    }) : Drupal.t('Double click to toggle relative time.');

    // Configure the timestamp accordingly.
    $element
      .attr('title', title)
      .timeago(timeagoEnabled ? undefined : 'dispose')
      .text(!timeagoEnabled ? createdDate : undefined);
  }

  Drupal.behaviors.drupalorgCrosssite = {
    attach: function (context) {
      $.extend($.timeago.settings, Drupal.settings.timeago);
      $(context).find('table').once('timeago', function () {
        var $times = $(this).find('time.timeago');
        $times
          // Initialize all timestamps.
          .each(toggleTimes)
          // Bind the "double click" event  to toggle all timestamps at the
          // same time. Not using the "click" event in case user wish to
          // specifically copy the date.
          .bind('dblclick', function () {
            timeagoEnabled = !timeagoEnabled;
            if (localStorage) {
              window.localStorage['drupalorgCommentTimeago'] = timeagoEnabled;
            }
            $times.each(toggleTimes);
          });
      });
    },
  };

})(jQuery)
;