# IjorTengab Module

Module ini berfungsi untuk memudahkan bagi saya pribadi untuk mengembangkan 
sistem/website dengan Drupal. Didalamnya terdapat berbagai fungsi API yang 
dapat digunakan untuk berbagai project. 

# IjorTengab Autologin Module

Submodule ini berfungsi untuk memudahkan login tanpa perlu mampir ke halaman
login. Setelah menginstall dan enable module ini, maka anda bisa login dengan
mengunjungi URL dengan format: /ijortengab/autologin/$username/$password
Contoh:
 - http://domain/path/to/drupal/ijortengab_autologin/budi/rahasiaku123
   Untuk login sebagai user "budi" dengan password "rahasiaku123".
 - http://domain/path/to/drupal/ijortengab_autologin/rahasiaku123
   Jika argument pada URL hanya satu yakni "rahasiaku123", maka ini berarti
   login sebagai user dengan uid "1" dengan password "rahasiaku123".

Sangat disarankan untuk menggunakan browser dengan mode "Private Window" (pada
Firefox Browser) atau "Incognito Window" (pada Chrome Browser) saat menggunakan
fitur autologin ini.

