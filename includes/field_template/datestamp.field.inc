array(
  'translatable' => '0',
  'entity_types' => array(),
  'settings' => array(
    'granularity' => array(
      'month' => 'month',
      'day' => 'day',
      'hour' => 'hour',
      'minute' => 'minute',
      'second' => 'second',
      'year' => 'year',
    ),
    'tz_handling' => 'site',
    'timezone_db' => 'UTC',
    'cache_enabled' => 0,
    'cache_count' => '4',
    'repeat' => '0',
    'todate' => '',
  ),
  'foreign keys' => array(),
  'indexes' => array(),
  'storage' => array(
    'type' => 'field_sql_storage',
    'settings' => array(),
    'module' => 'field_sql_storage',
    'active' => '1',
    'details' => array(
      'sql' => array(
        'FIELD_LOAD_CURRENT' => array(
          'field_data_field_tanggkitasemua' => array(
            'value' => 'field_tanggkitasemua_value',
          ),
        ),
        'FIELD_LOAD_REVISION' => array(
          'field_revision_field_tanggkitasemua' => array(
            'value' => 'field_tanggkitasemua_value',
          ),
        ),
      ),
    ),
  ),
  'id' => '258',
  'field_name' => 'field_tanggkitasemua',
  'type' => 'datestamp',
  'module' => 'date',
  'active' => '1',
  'locked' => '0',
  'cardinality' => '1',
  'deleted' => '0',
  'columns' => array(
    'value' => array(
      'type' => 'int',
      'not null' => FALSE,
      'sortable' => TRUE,
      'views' => TRUE,
    ),
  ),
  'bundles' => array(
    'node' => array(
      'test',
    ),
  ),
)