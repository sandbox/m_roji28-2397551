<?php
/**
 * @file
 * Provide API to working with roles.
 *
 * Give you some benefit function about roles.
 * This module using machine name of role by default
 * if you enabled module role_export.
 */

class role {
  /**
   * Check if the user has a role.
   *
   * @param $role mixed
   *   A machine name of role that build form module role_export if.
   *   parameter $machine_name set TRUE, or label of role otherwise.
   * @param $user
   *   Flexible parameter. If the value's argument is NULL, it means current
   *   user (load from $GLOBALS['user']). The value's argument may as
   *   numeric (it means uid) or string (it means username) or user object.
   * @param $machine_name
   *   Depends by $role parameter.
   * @param $raw
   *   If set as TRUE, user's role will check direct from database not
   *   from $GLOBALS['user'], because of a contributed module may modify
   *   of user's role information in $GLOBALS['user'].
   *
   * @return
   *   Returns TRUE if user has that role, FALSE otherwise.
   */
  public static function hasRole($role, $user = NULL, $machine_name = TRUE, $raw = FALSE) {
    // This method function may use in hook_menu(), that we need
    // logic about global menu_admin, @see user_is_anonymous().
    if (!empty($GLOBALS['menu_admin'])) {
      return TRUE;
    }
    if (!module_exists('role_export')) {
      // Force parameter $machine_name to FALSE.
      $machine_name = FALSE;
    }
    $user = self::user_load($user);
    // $debugname = 'user'; debug_ijor($$debugname, '$' . $debugname, 'f:__1');
    $user_roles = $raw ? self::user_roles_raw($user, $machine_name) : self::user_roles($user, $machine_name);
    if ($machine_name) {
      return isset($user_roles[$role]);
    }
    else {
      return in_array($role, $user_roles);
    }
  }

  /**
   * Add a role to the user.
   *
   * @param $role_machine_name
   *   A machine name of role that build form module role_export.
   * @param $user
   *   Flexible parameter. If the value's argument is NULL, it means current user
   *   (load from $GLOBALS['user']). The value's argument may as numeric (it means
   *   uid) or string (it means username) or user object.
   */
  public static function addRole($role, $user = NULL, $machine_name = TRUE) {
    list($rid, $name) = self::getInfo($role, $machine_name);
    $user = self::user_load($user);
    if ($rid && $user) {
      $user_roles = self::user_roles_raw($user, FALSE);
      if (!isset($user_roles[$rid])) {
        $user_roles[$rid] = $name;
        return user_save($user, array('roles' => $user_roles));
      }
    }
  }

  /**
   * Remove a role from the user.
   *
   * @param $role_machine_name
   *   A machine name of role that build form module role_export.
   * @param $user
   *   Flexible parameter. If the value's argument is NULL, it means current user
   *   (load from $GLOBALS['user']). The value's argument may as numeric (it means
   *   uid) or string (it means username) or user object.
   */
  public static function removeRole($role, $user = NULL, $machine_name = TRUE) {
    list($rid, $name) = self::getInfo($role, $machine_name);
    $user = self::user_load($user);
    if ($rid && $user) {
      $user_roles = self::user_roles_raw($user, FALSE);
      if (isset($user_roles[$rid])) {
        unset($user_roles[$rid]);
        return user_save($user, array('roles' => $user_roles));
      }
    }
  }

  /**
   * Get list roles of user.
   *
   * @param $user
   *   Flexible parameter. If the value's argument is NULL, it means current
   *   user (load from $GLOBALS['user']). The value's argument may as
   *   numeric (it means uid) or string (it means username) or user object.
   *
   * Return
   *   Array with key rid if parameter $machine_name set as FALSE, or
   *   key machine_name otherwise.
   */
  public static function infoRole($user = NULL, $machine_name = TRUE) {
    if (!module_exists('role_export')) {
      // Force parameter $machine_name to FALSE.
      $machine_name = FALSE;
    }
    $user = self::user_load($user);
    return self::user_roles_raw($user, $machine_name);
  }

  /**
   * Get rid of role.
   */
  protected static function getInfo($role, $machine_name = TRUE) {
    if (!module_exists('role_export')) {
      // Force parameter $machine_name to FALSE.
      $machine_name = FALSE;
    }
    $rid = NULL;
    $name = NULL;
    if ($machine_name) {
      $load = self::is_machine_name_exists($role);
      if ($load) {
        $rid = $load->rid;
        $name = $load->name;
      }
    }
    else {
      $user_roles = user_roles();
      if (in_array($role, $user_roles)) {
        $rid = array_search($role, $user_roles);
        $name = $user_roles[$rid];
      }
    }
    return array($rid, $name);
  }

  /**
   * Duplikat dari fungsi ijortengab_load_user().
   */
  protected static function user_load($user) {
    $argument = $user;
    if (is_null($user)) {
      $argument = $GLOBALS['user']->uid;
    }
    if (is_object($user) && isset($user->uid)) {
      $argument = $user->uid;
    }

    static $info = array();
    if (!isset($info[$argument])) {
      $info[$argument] = FALSE;
      if (is_null($user)) {
        $info[$argument] = $GLOBALS['user'];
      }
      elseif (is_numeric($user)) {
        $info[$argument] = user_load($user);
      }
      elseif (is_string($user)) {
        $info[$argument] = user_load_by_name($user);
      }
      elseif (is_object($user) && isset($user->uid)) {
        $info[$argument] = $user;
      }
    }
    return $info[$argument];
  }

  /**
   * Return information about user's role.
   *
   * Source of user's role is from $GLOBALS['user'].
   *
   * @param $user
   *   User object.
   * @param $machine_name
   *   If set as TRUE, keys of array will convert to machine name instead of rid.
   *
   * @return
   *   An array of user's role. The key may rid or machine name depends of
   *   $machine_name paramater. The value is label/human-readeable of role.
   */
  protected static function user_roles($user, $machine_name = TRUE) {
    if (!isset($user->uid)) {
      return FALSE;
    }
    static $info = array();
    $argument = $user->uid . '-' . (int) $machine_name;

    if (!isset($info[$argument])) {
      if ($machine_name) {
        $role_export_roles = role_export_roles();
        $_user_roles = $user->roles;
        $user_roles = array();
        foreach ($_user_roles as $rid => $role_label) {
          if (!empty($role_export_roles[$rid]->machine_name)) {
            $role_machine_name = $role_export_roles[$rid]->machine_name;
            $user_roles[$role_machine_name] = $role_label;
          }
          else{
            $user_roles[$rid] = $role_label;
          }
        }
        $info[$argument] = $user_roles;
      }
      else{
        $info[$argument] = $user->roles;
      }
    }
    return $info[$argument];
  }

  /**
   * Return information about user's role.
   *
   * Source of user's role is from database not from $GLOBALS['user'], because of
   * a contributed module may modify of user's role information in
   * $GLOBALS['user'].
   *
   * @param $user
   *   User object.
   * @param $machine_name
   *   If set as TRUE, keys of array will convert to machine name instead of rid.
   *
   * @return
   *   An array of user's role. The key may rid or machine name depends of
   *   $machine_name paramater. The value is label/human-readeable of role.
   */
  protected static function user_roles_raw($user, $machine_name = TRUE) {
    if (!isset($user->uid) || $user->uid == 0) {
      return FALSE;
    }
    $user_roles = array();
    $user_roles[DRUPAL_AUTHENTICATED_RID] = 'authenticated user';
    if ($machine_name) {
      $_user_roles = db_query("SELECT r.rid, r.machine_name, r.name FROM {role} r INNER JOIN {users_roles} ur ON ur.rid = r.rid WHERE ur.uid = :uid", array(':uid' => $user->uid))->fetchAll();
      foreach ($_user_roles as $i => $object) {
        if (!empty($object->machine_name)) {
          $user_roles[$object->machine_name] = $object->name;
        }
        else {
          $user_roles[$object->rid] = $object->name;
        }
      }
    }
    else {
      $user_roles += db_query("SELECT r.rid, r.name FROM {role} r INNER JOIN {users_roles} ur ON ur.rid = r.rid WHERE ur.uid = :uid", array(':uid' => $user->uid))->fetchAllKeyed(0, 1);
    }

    return $user_roles;
  }

  /**
   * Check if role machine name exists then load role object.
   *
   * @param $role_machine_name
   *   A machine name of role that build form module role_export.
   *
   * @return
   *   Role object if exists or NULL otherwise.
   */
  protected static function is_machine_name_exists($machine_name) {
    if (!module_exists('role_export')) {
      return;
    }
    $role_export_roles = role_export_roles();
    // $debugname = 'role_export_roles'; dpm($$debugname, '$' . $debugname);
    foreach ($role_export_roles as $object) {
      if ($object->machine_name == $machine_name) {
        return $object;
      }
    }
  }

}