<?php



class convert {
  public static function strtotime_format_webform($input, $gmt = '+0700') {
    preg_match('/(.*)\/(.*)\/(.*) - (.*)/', $input, $m); // misal 19/12/2013 - 19:27 (tidak ada info gmt=+7)
    if (count($m) == 5) {
      $output = $m[3] . '-' . $m[2] . '-' . $m[1] .' ' . $m[4] . ' ' .  $gmt; // 2013-12-19 19:27 +0700
      return strtotime($output);
    }
  }
}
