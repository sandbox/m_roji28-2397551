<?php

function ijortengab_autologin_page() {
  if (user_is_logged_in()) {
    drupal_goto();
  }

  $superuser = FALSE;
  $username = arg(2);
  if (empty($username)) {
    return drupal_not_found();
  }
  $password = arg(3);
  if (empty($password)) {
    $superuser = TRUE;
    $password = $username;
    $username = user_load(1)->name;
  }
  elseif ($username == user_load(1)->name) {
    $superuser = TRUE;
  }
  
  // Send form programmatically.
  $form_state = array(
    'values' => array(
      'name' => $username,
      'pass' => $password,
    ),
  );
  drupal_form_submit('user_login', $form_state);
  
  // If success.
  $errors = form_get_errors();
  if (empty($errors)) {
    // Redirect to home.
    return ijortengab_autologin_success();
  }
  // Hapus error sebelumnya.
  unset($_SESSION['messages']['error']);

  // Jika ada error dan error tidak disebabkan flooding, maka
  // kita ijinkan masuk login dengan secondary password.
  $is_flood = isset($form_state['flood_control_triggered']);
  $is_secondary_password_enabled = variable_get('ijortengab_autologin_secondary_password_enabled');
  if ($superuser && $is_secondary_password_enabled && !$is_flood) {
    // Make sure that $secondary_password not deleted by accident.
    if ($secondary_password = variable_get('ijortengab_autologin_secondary_password_value')) {
      // Matching.
      if ($secondary_password === $password) {
        // Success
        drupal_set_message(t('Login using secondary password.'));
        // Hapus flood yang baru saja terjadi yang dibuat oleh
        // validator dari form user_login.
        db_delete('flood')
          ->condition('timestamp', REQUEST_TIME)
          ->execute();

        // Lets login.
        global $user;
        $user = user_load_by_name($username);
        user_login_finalize();
        return ijortengab_autologin_success();
      }
    }
  }

  // Login gagal, maka kita perlu pura2 bahwa module ini tidak pernah terinstall
  return drupal_not_found();
}

function ijortengab_autologin_success() {
  drupal_set_message(t('Success login via IjorTengab Autologin Module'));
  drupal_set_message(t('This module dangerous, not recommended use for production site.'), 'error');
  drupal_set_message(t('Uninstall this module immediatley if working finish in development site.'), 'warning');
  drupal_goto();
}
