<?php

function ijortengab_autologin_form($form, &$form_state) {
  $name = user_load(1)->name;
  $form = array();
  $form['admin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Another password for user#1 (%name).', array('%name' => $name)),
    '#collapsible' => TRUE,
  );
  $form['admin']['ijortengab_autologin_secondary_password_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Secondary Password'),
    '#default_value' => variable_get('ijortengab_autologin_secondary_password_enabled', FALSE),    
    '#description' => t('If you still worry to login via URL even run in development site (eq: You use sharing computer which browser save your history URL), you can put another password here to login as user#1 (%name) via URL.', array('%name' => $name)),
  );
  $form['admin']['ijortengab_autologin_secondary_password_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Secondary Password'),
    '#default_value' => variable_get('ijortengab_autologin_secondary_password_value', ''),
    '#element_validate' => array('ijortengab_autologin_secondary_password_validate'),    
    '#field_suffix' => '<a href="javascript:void(0);" id="anchorpassword">Generate Password</a>',
    '#states' => array(
      'visible' => array(
        'input[name=ijortengab_autologin_secondary_password_enabled]' => array('checked' => TRUE),
      ),
      'required' => array(
        'input[name=ijortengab_autologin_secondary_password_enabled]' => array('checked' => TRUE),
      ),
    ),
  );  

  $form['#attached']['js'][] = drupal_get_path('module', 'ijortengab') . '/js/pGenerator/jquery.pGenerator.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'ijortengab') . '/js/pGenerator/drupal.pGenerator.js';
  $form['#attached']['js'][] = array(
    'data' => array(
      'pGenerator' => array(
        array(
          'feeder' => '#anchorpassword',
          'target' => 'input[name="ijortengab_autologin_secondary_password_value"]',
        ),
      ),
    ),
    'type' => 'setting',
  );
  
  // $debugname = 'form'; dpm($$debugname, '$' . $debugname);
  return system_settings_form($form);
}

function ijortengab_autologin_secondary_password_validate(&$element, &$form_state) {
  // $debugname = 'form_state'; dpm($$debugname, '$' . $debugname);
  $enabled = $form_state['values']['ijortengab_autologin_secondary_password_enabled'];
  $value = trim($form_state['values']['ijortengab_autologin_secondary_password_value']);
  if (!empty($enabled) && empty($value)) {
    form_set_error('ijortengab_autologin_secondary_password_value', t('Secondary Password cannot empty.'));
  }
}
