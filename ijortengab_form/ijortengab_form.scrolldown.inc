<?php
/**
 * @file
 *   Provide a way to create form with scrolldown step.
 */

/**
 * Primary function to modify Drupal's form;
 */
function ijortengab_form_scrolldown_modify(&$form, &$form_state) {  
  $form['#attached']['js'][] = drupal_get_path('module','ijortengab_form') . '/ijortengab_form.scrolldown.js';
  if (!isset($form_state['current_step'])) {
    $step_max = 1;
    $form_state['current_step'] = 1;
    $form_state['step_reached'] = 1;
    foreach (element_children($form) as $element) {
      if (isset($form[$element]['#step']) && is_numeric($form[$element]['#step'])) {
        $this_step = (int) $form[$element]['#step'];
        if ($this_step > $step_max) {
          $step_max = $this_step;
        }
        if ($this_step > 1) {
          $form[$element]['#access'] = FALSE;
        }
      }
    }
    $form_state['step_max'] = $step_max;
  }

  $step_max = $form_state['step_max'];
  // Modifikiasi element.
  if ($step_max > 1) {
    // Tambah attribute #ajax.
    _ijortengab_form_scrolldown_modify($form, $form_state);

    // Buat element tambahan.
    for ($x=2; $x <= $step_max; $x++) {
      $name = 'step_' . $x;
      $id = 'step-' . $x;
      // Buat element wrapper.
      $form[$name] = array(
        '#prefix' => '<div id="'.  $id .'">',
        '#suffix' => '</div>',
      );
    }
    $form['msg'] = array(
      '#markup' => '<div id="msg"></div>',
      '#weight' => -1,
    );
  }


  // Ajax coming.
  // $debugname = 'form_state'; ijortengab_debug($$debugname, '$' . $debugname, 'f:__1');
  if (isset($form_state['triggering_element']['#step'])) {

    $form_state['current_step'] = $form_state['triggering_element']['#step'];
    if ($form_state['current_step'] > $form_state['step_reached']) {
      $form_state['step_reached'] = $form_state['current_step'];
    }

    $current_step = &$form_state['current_step'];
    $step_reached = &$form_state['step_reached'];
    $step_max = $form_state['step_max'];

    // Reset.
    $form_state['return_element'] = $form_state['remove_element'] = array();

    $element_required_is_empty = ijortengab_form_scrolldown_validate_current_step($form_state);

    $key = empty($element_required_is_empty) ? 'return_element' : 'remove_element';
    // dpm($key,'$key');

    switch ($key) {
      case 'return_element':
        do {
          $current_step++;
          // dpm($current_step,'$current_step');
          // dpm($step_reached,'$step_reached');
          // dpm($step_max,'$step_max');
          // Get element per step.
          $_form = ijortengab_form_scrolldown_get_element_in_step($current_step, $form, $form_state);
          // dpm($_form,'$_form BEFORE');
          // Untuk step yang belum pernah dibuka yang ditandai dengan logika:
          // $current_step >= $step_reached, juga step selain step terakhir,
          // maka perlu difilter properti
          // #akses-nya, jika bernilai FALSE maka perlu dihapus agar dapat
          // skip ke step berikutnya.
          if (!empty($_form) && $current_step >= $step_reached && $step_reached != $step_max) {
            foreach (element_children($_form) as $element) {
              $access = isset($_form[$element]['#access']) ? $_form[$element]['#access'] : TRUE;
              if ($access !== TRUE) {
                unset($_form[$element]);
              }
            }
          }
          // dpm($_form,'$_form AFTER');

          // Jika ada step yang kosong, maka paksa untuk melewatinya.
          if (empty($_form) && $current_step >= $step_reached) {
            $form_state[$key][] = $current_step;
            if ($step_max - $step_reached >= 2) {
              $step_reached += 2;
            }
          }
          // Selain itu, maka:
          else {
            $form_state[$key][] = $current_step;
            if ($current_step > $step_reached) {
              // Update info, jika element yg direturn lebih tinggi dari step reached.
              $step_reached = $current_step;
            }
          }
        }
        while ($current_step < $step_reached && $step_reached <= $step_max);
        break;

      case 'remove_element':
        while ($current_step < $step_reached && $step_reached <= $step_max) {
          $current_step++;
          $_form = ijortengab_form_scrolldown_get_element_in_step($current_step, $form, $form_state);
          // Jika ada step yang kosong, maka paksa untuk melewatinya.
          empty($_form) or $form_state[$key][] = $current_step;
        }
        break;
    }

    // dpm($form,'$form');
    // dpm($form_state,'$form_state');


    // Buang untuk selalu terdapat value baru.
    if (isset($form_state['complete form'])) {
      foreach($form_state['complete form'] as $element => $info) {
        if (isset($info['#step'])) {
          unset($form_state['complete form'][$element]);
        }
      }
    }
    // dpm($form_state,'$form_state');

  }
}

function _ijortengab_form_scrolldown_modify(&$form, &$form_state) {

  foreach (element_children($form) as $element) {
    // Hanya yang ada step, dan status step adalah TRUE dan tidak disabled.
    $access = isset($form[$element]['#access']) ? $form[$element]['#access'] : TRUE;
    $disabled = isset($form[$element]['#disabled']) ? $form[$element]['#disabled'] : FALSE;
    if ((isset($form[$element]['#step']) && $access && !$disabled) === FALSE) {
      continue;
    }
    // Tambahkan property #ajax.
    if (in_array($form[$element]['#type'], array('select', 'radios', 'checkboxes', 'textfield', 'textarea', 'password'))) {      
      $form[$element]['#ajax'] = array(
        'callback' => 'ijortengab_form_scrolldown_ajax_callback',
      );
    }
    
    // Tambahkan css class pada input text sebagai trigger bagi javascript.
    if (in_array($form[$element]['#type'], array('textfield', 'textarea', 'password'))) {
      isset($form[$element]['#attributes']['class']) or $form[$element]['#attributes']['class'] = array();
      $form[$element]['#attributes']['class'] = array_merge($form[$element]['#attributes']['class'], array('ijortengab-form-scrolldown-input-text'));
    }
    
    // Khusus untuk radios, maka copykan attribute step ke anak.
    $step = $form[$element]['#step'];
    if (in_array($form[$element]['#type'], array('radios')) && !empty($form[$element]['#options'])) {
      foreach ($form[$element]['#options'] as $option => $label) {
        $form[$element][$option]['#step'] = $step;
      }
    }
  }

  // $debugname = 'form'; ijortengab_debug($$debugname, '$' . $debugname, 'f:__1');
  // $debugname = 'form_state'; ijortengab_debug($$debugname, '$' . $debugname, 'f:__1');


// for ($x=1; $x <= $step_max; $x++) {
    // $_form = ijortengab_form_scrolldown_get_element_in_step($x, $form, $form_state);
    // debug_ijor($aku);
  // }
  /* $element_by_step = array(); */
/*
  // Special for textfield.
  // jika terdapat field text dan radios, maka
  // field text dan radios bisa bentrok
  // {berikan contoh kasus}
  // , sehingga perlu diberikan antisipasi.
  // debug_ijor($element_by_step, '$element_by_step');
  if (!empty($element_by_step)) {
    foreach ($element_by_step as $step => $info) {
      $add_ajax = FALSE;
      if (!empty($info['text']) && !empty($info['radio'])) {
        // pada radio kita berikan status disabled
        // Simpan informasi
        // $count_text = count($info['text']);
        // if () {}
        // debug_ijor($step, '$step');
      }
    }
  } */
}

function ijortengab_form_scrolldown_validate_current_step($form_state) {
  // Gunakan complete form, karena sudah diproses, sehingga attribute element lebih lengkap.
  $form = ijortengab_form_scrolldown_get_element_in_step('current', $form_state['complete form'], $form_state);
  $element_required_is_empty = array();
  foreach (element_children($form) as $sub_element) {
    $elements = $form[$sub_element];
    // Sumber code dibawah ini dari fungsi _form_validate().
    if (isset($elements['#needs_validation']) && $elements['#required']) {
      $is_empty_multiple = (!count($elements['#value']));
      $is_empty_string = (is_string($elements['#value']) && drupal_strlen(trim($elements['#value'])) == 0);
      $is_empty_value = ($elements['#value'] === 0);
      if ($is_empty_multiple || $is_empty_string || $is_empty_value) {
        $element_required_is_empty[$sub_element] = $elements;
      }
    }
  }
  return $element_required_is_empty;
}

function ijortengab_form_scrolldown_get_element_in_step($which, $form, $form_state) {
  switch ($which) {
    case 'current':
      $step = $form_state['current_step'];
      break;
    default:
      $step = $which;
      break;
  }
	$_form = array();
  foreach (element_children($form) as $element) {
    if (isset($form[$element]['#step']) && $form[$element]['#step'] == $step) {
      // isset($form[$element]['#access']) or $form[$element]['#access'] = TRUE;
      // debug_ijor($form[$element], '$form[$element]', 'f:__1');
      // if ($form[$element]['#access']) {
        $_form[$element] = $form[$element];
      // }
    }
  }
  // dpm($_form,'$_form');
  return $_form;
}

function ijortengab_form_scrolldown_ajax_callback($form, $form_state) {
  // $debugname = 'form'; ijortengab_debug($$debugname, '$' . $debugname, 'f:__1');
  // $debugname = 'form_state'; ijortengab_debug($$debugname, '$' . $debugname, 'f:__1');

  $result = array('#type' => 'ajax');
  if (!empty($form_state['return_element'])) {
    foreach($form_state['return_element'] as $current_step) {
      $_form = ijortengab_form_scrolldown_get_element_in_step($current_step, $form, $form_state);
      $_form['#prefix'] = '<div id="step-'.  $current_step .'">';
      $_form['#suffix'] = '</div>';
      $result['#commands'][] = ajax_command_replace( '#step-' . $current_step, drupal_render($_form));
    }
  }
  if (!empty($form_state['remove_element'])) {
    foreach($form_state['remove_element'] as $current_step) {
      $_form = array('#markup' => '');
      $_form['#prefix'] = '<div id="step-'.  $current_step .'">';
      $_form['#suffix'] = '</div>';
      $result['#commands'][] = ajax_command_replace( '#step-' . $current_step, drupal_render($_form));
    }
  }

  // debug_ijor($form_state, '$form_state', 'f:__1');


  $result['#commands'][] = ijortengab_form_scrolldown_command_print_message();
  // debug_ijor($result, '$result', 'f:__1');
  return $result;
}

function ijortengab_form_scrolldown_command_print_message() {
  $element = array(
    '#prefix' => '<div id="msg">',
    '#suffix' => '</div>',
    '#markup' => theme('status_messages'),
  );
  return ajax_command_replace('#msg', drupal_render($element));
}
