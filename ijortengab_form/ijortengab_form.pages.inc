<?php

function ijortengab_form_sample() {
  return 'Click on Tab Form 1, Form 2, etc...';
}

function ijortengab_form_sample_form($form, &$form_state, $n) {
  $form['help']['#markup'] = 'Hai.' . $n;
  $f = 'ijortengab_form_sample_form_' . $n;
  if (!function_exists($f)) {
    return $form;
  }
  $_form = $f($form, $form_state);
  if (!isset($form_state['ijortengab_form'])) {
    eval("\$form_state['ijortengab_form'] = " . $_form);
  }
  $form = $form_state['ijortengab_form'];
  $form['__info'] = array(
    '#weight' => -100,
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'Show Code',
    'html' => array(    
      '#markup' => '<pre> $form = ' . $_form . '</pre>',
    ),
  );
  return $form;
}

function ijortengab_form_sample_form_1($form, &$form_state) {
  // Define form.
  return <<<HEREDOC
array(
  // Beri informasi kepada module ijortengab_form
  // bahwa form ini menggunakan fitur scrolldown.
  '#ijortengab_form' => 'scrolldown',
  // Tiap element perlu ada property #step
  // untuk bisa dikenali sebagai bagian dari multistep scrolldown.
  'kelamin' => array(
    '#title' => 'Jenis Kelamin',
    '#type' => 'radios',
    '#options' => drupal_map_assoc(array('L','P')),
    '#step' => 1,
    '#description' => 'This is step 1.',
  ),
  'nama' => array(
    '#title' => 'Nama',
    '#type' => 'textfield',
    '#step' => 2,
    '#description' => 'This is step 2.',
  ),
  'submit' => array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#step' => 3,
    '#description' => 'This is step 3.',
  ),
);
HEREDOC;
  
}

function ijortengab_form_sample_form_submit($form, $form_state) {
  $file = DRUPAL_ROOT . DIRECTORY_SEPARATOR . 'includes' .  DIRECTORY_SEPARATOR . 'utility.inc';  
  if (is_file($file)) {
    require_once($file);    
    $string = drupal_var_export($form_state['values']);
    drupal_set_message('<pre> $form_state[\'values\'] = ' . $string . ';</pre>');
  }
}