(function ($) {
/**
 * Jika dalam satu form terdapat input radio dan input text, maka
 * akan terjadi konflik event pada javascript. Misalnya user melakukan
 * event focus() pada input text, kemudian user melakukan event dblur()
 * pada input text sekaligus/berbarengan dengan melakukan event click()
 * pada input radio, ini menyebabkan ajax post yang dikirim tidak
 * memberikan respon. Sehingga kita melakukan trik, yakni ketika user
 * melakukan event focus() pada input text, maka semua input radio akan
 * disabled, lalu ketika user melakukan event dblur(), maka akan diberi
 * jeda sesaat (250 ms) agar input radio kembali aktif dapat diklik
 * dengan menghapus attribut disabled-nya.
 *
 * Variable focus dan dblur digunakan jika terdapat lebih dari satu element text.
 * yang mana juga akan bentrok jika mereka ... todo lengkapi document ini
 */

Drupal.ijortengabDatabase = function (){
  this.focus = 0
  this.blur = 0
  // var a = 0
  // var b = 0
}
Drupal.ijortengabText = function (element, db){
  // var a = 0
  // var b = 0
  // console.log(element);
  // console.log(db);
  var self = this;
  var $element = $(element);
  this.$element = $element;
  this.db = db;
  $element.focus(function() {
    return self.onfocus();
  });
  $element.blur(function() {
    return self.onblur();
  });
}
Drupal.ijortengabText.prototype.onfocus = function() {
  // console.log(this);
  this.db.focus++;
  // Scan all element of radio, then disabled all.
  var $radios = this.$element.parents('form').find('input[type="radio"]');
  $radios.each(function(){
    if (this.checked && $(this).next('input.dummy-text').length == 0) {
      // Buat input dummy text sekedar untuk mengalihkan pembicaraan.
      $('<input>').attr({'class':'dummy-text', 'type':'text', 'name':this.name, 'value':this.value}).hide().insertAfter(this);
    }
    this.disabled = true;
  });
  // Scan all element of button, then disabled all.
  var $submit = this.$element.parents('form').find('input[type="submit"]');
  $submit.each(function(){
    if ($(this).next('input.dummy-text').length == 0) {
      // Buat input dummy text sekedar untuk mengalihkan pembicaraan.
      // $('<input>').attr({'class':'dummy-text', 'type':'text', 'name':this.name, 'value':this.value}).hide().insertAfter(this);
    }
    this.disabled = true;
  });
}

Drupal.ijortengabText.prototype.onblur = function() {
  this.db.blur++;
  // console.log(this);
  // console.log(this.db.focus);
  // console.log(this.db.blur);
  var self = this
  setTimeout(function(){
    if (self.db.focus == self.db.blur) {
      var $radios = self.$element.parents('form').find('input[type="radio"]');
      $radios.each(function(){
        this.disabled = false;
        $(this).next('input.dummy-text').remove();
      });
      var $submit = self.$element.parents('form').find('input[type="submit"]');      
      $submit.each(function(){
        this.disabled = false;
        $(this).next('input.dummy-text').remove();
      });
    }
    // console.log('timeout');
    // console.log(self.db.focus);
    // console.log(self.db.blur);

  }, 500);

}
Drupal.behaviors.ijortengabFormScrolldown = {
	attach: function (context, settings) {
    if (typeof ijortengabDatabase == "undefined") {
      // console.log('belum ada');
      ijortengabDatabase = new Drupal.ijortengabDatabase();
    }
    // console.log(tempe);
    // var self = this;
    // this.focus = 0;
    // this.dblur = 0;
    $('input[type="radio"]', context).once('ijorsdfatengab', function() {
      // console.log('aku');s
      // console.log('COMING');
      // console.log(self.focus);
      // console.log(self.dblur);
      // if (self.focus != self.dblur) {
        // this.disabled = true;
      // }
    });
    $('.ijortengab-form-scrolldown-input-text').once('ijortengab', function() {
      new Drupal.ijortengabText(this, ijortengabDatabase);
      // console.log(adfasd);
      /* $(this)
      .focus(function(){
        self.focus++;
        console.log('fokus');
        console.log(self.focus);
        console.log(self.dblur);
        // Cari input type radio.
        var $radios = $(this).parents('form').find('input[type="radio"]');
        $radios.each(function(){
          // if (this.checked && $(this).next('input[type="text"]').length == 0) {
          if (this.checked) {
            // Buat input dummy text sekedar untuk mengalihkan pembicaraan.
            $('<input>').attr({'type':'text', 'name':this.name, 'value':this.value}).hide().insertAfter(this);
          }
          this.disabled = true;
        });
      })
      .blur(function(){
        // self.dblur++;
        console.log(this);
        console.log(self);
        return;
        console.log('dblur');
        console.log(self.focus);
        console.log(self.dblur);
        var self = this;
        // this.disabled = true;
        setTimeout(function(){
          if (self.focus == self.dblur) {
            var $radios = $(self).parents('form').find('input[type="radio"]');
            $radios.each(function(){
              this.disabled = false;
              $(this).next('input[type="text"]').remove();
            });
          }
          console.log('timeout');
          console.log(self.focus);
          console.log(self.dblur);

        }, 500);
      }); */
    });
	},
};
})(jQuery);
