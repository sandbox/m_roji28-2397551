<?php

function ijortengab_field_create_field_form($form, &$form_state) {

  if (!isset($form_state['create_field_template'])) {
    $form_state['create_field_template'] = ijortengab_pages_create_field_scan_template();
  }

  if (!isset($form_state['ijortengab'])) {
    $form_state['ijortengab']['entity info'] = array();
    // Get info of entity and it's bundle.
    foreach(entity_get_info() as $entity_name => $entity_info) {
      $form_state['ijortengab']['entity info']['entity options'][$entity_name] = $entity_info['label'];
      foreach($entity_info['bundles'] as $bundle_name => $bundle_info) {
        $form_state['ijortengab']['entity info']['bundle options'][$entity_name][$bundle_name] = $bundle_info['label'];
      }
    }
    // Get info of field.
    $fields = field_info_fields();
    array_multisort(array_keys($fields), SORT_NATURAL, $fields);
    $opt = array();
    foreach ($fields as $field => $info) {
      foreach ($info['bundles'] as $entity => $list_bundle) {
        foreach ($list_bundle as $bundle) {
          $key = $entity . '][' . $field . '][' . $bundle;
          $label = $field . ' (' . $entity . '/' . $bundle . ')';
          $opt[$key] = $label;
        }
      }
    }
    $form_state['ijortengab']['entity info']['field options'] = $opt;
  }

  $form['field_name'] = array(
    '#title' => 'Field Name',
    '#type' => 'machine_name',
    '#machine_name' => array('exists' => 'ijortengab_pages_create_field_exists_field_name'),
    '#maxlength' => '32',
    '#required' => TRUE,
  );
  $form['source'] = array(
    '#title' => 'Select Source of field',
    '#description' => t('Select the source of field.'),
    '#type' => 'radios',
    '#step' => 1,
    '#disabled' => false,
    '#required' => TRUE,
    '#options' => array(
      'template' => t('Template'),
      'exists' => t('Existing Field'),
    ),
  );
  $form['template'] = array(
    '#title' => 'Select Template',
    '#description' => t('You can add your own template. More Info.'),
    '#type' => 'select',
    '#step' => 3,
    '#required' => TRUE,
    '#options' => drupal_map_assoc(array_keys($form_state['create_field_template'])),
    '#access' => isset($form_state['values']['source']) && $form_state['values']['source'] == 'template',
  );
  $form['exists'] = array(
    '#title' => 'Select Field',
    '#description' => 'Select field that exists.',
    '#type' => 'select',
    '#step' => 3,
    '#required' => TRUE,
    '#options' => $form_state['ijortengab']['entity info']['field options'],
    '#access' => isset($form_state['values']['source']) && $form_state['values']['source'] == 'exists',
  );
  $form['entity'] = array(
    '#title' => 'Select Entity',
    '#description' => 'Target destination of entity to create field.',
    '#type' => 'select',
    '#step' => 5,
    '#required' => TRUE,
    '#options' => $form_state['ijortengab']['entity info']['entity options'],
    '#access' =>
      ($form['template']['#access'] && !empty($form_state['values']['template'])) ||
      ($form['exists']['#access'] && !empty($form_state['values']['exists'])),
  );
  $bundles_options = array();
  if (!empty($form_state['values']['entity'])) {
    $bundles_options = $form_state['ijortengab']['entity info']['bundle options'][$form_state['values']['entity']];
  }
  $form['bundle'] = array(
    '#title' => 'Select Bundle',
    '#description' => 'Target destination of bundle from entity above to create field.',
    '#type' => 'select',
    '#step' => 6,
    '#required' => TRUE,
    '#options' => $bundles_options,
    '#access' => $form['entity']['#access'] && !empty($form_state['values']['entity']),
  );
  // $form['actions'] = array(
    // '#type' => 'actions',
    // '#weight' => 2,
    // '#step' => 7,
    // 'submit' => array(
      // '#type' => 'submit',
      // '#value' => t('Save'),
    // ),
  // );
  $form['actions'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#step' => 7,    
  );

  // Set a sign for module ijortengab_form.
  // Build as multistep scroll down.
  $form['#ijortengab_form'] = 'scrolldown';
  return $form;
}

function ijortengab_pages_create_field_scan_template() {
  // Cari file dengan format TYPE.field.inc.
  $files = file_scan_directory(__DIR__ . '/includes/field_template', '/.field\.inc$/');
  $template = array();
  foreach ($files as $path_field => $info) {
    // Harus ada juga file TYPE.instance.inc.
    $path_instance = preg_replace('/field\.inc$/', 'instance.inc', $path_field);
    if (file_exists($path_field) && file_exists($path_instance)) {
      // Load kedua file tersebut.
      $field = '';
      $instance = '';
      $contents_field = file_get_contents($path_field);
      $contents_instance = file_get_contents($path_instance);
      ob_start();
      eval("\$field = $contents_field;");
      eval("\$instance = $contents_instance;");
      ob_end_clean();
      if (is_array($field) && is_array($instance) && isset($field['type']) && isset($field['module']) && module_exists($field['module'])) {
        // Template Valid, maka jadikan sebagai opsi.
        $type = $field['type'];
        // Hindari duplikat.
        if (!isset($template[$type])) {
          $template[$type] = array(
            'field' => $field,
            'instance' => $instance,
          );
        }
      }
    }
  }
  return $template;
}

function ijortengab_pages_create_field_exists_field_name($value) {
  return (bool) field_read_fields(array('field_name' => $value), array('include_inactive' => TRUE));
}

function ijortengab_field_create_field_form_submit(&$form, &$form_state) {
  switch ($form_state['values']['source']) {
    case 'exists':
      list($entity, $field, $bundle) = explode('][', $form_state['values']['exists']);
      $field_info = field_info_field($field);
      $field_instance = field_info_instance($entity, $field, $bundle);
      break;

    case 'template':
      $field_info = $form_state['create_field_template'][$form_state['values']['template']]['field'];
      $field_instance = $form_state['create_field_template'][$form_state['values']['template']]['instance'];
      break;
  }
  if (!isset($field_info) || !isset($field_instance)) {
    drupal_set_message('EROOR', 'error');
    return false;
  }

  // Modify $field_info.
  unset($field_info['foreign keys']);
  unset($field_info['indexes']);
  unset($field_info['storage']);
  unset($field_info['entity_types']);
  unset($field_info['columns']);
  unset($field_info['bundles']);
  $field_info['field_name'] = $form_state['values']['field_name'];

  // Modify $field_instance.
  $_field_name = $field_instance['field_name'] = $form_state['values']['field_name'];
  $_entity_type = $field_instance['entity_type'] = $form_state['values']['entity'];
  $_bundle = $field_instance['bundle'] = $form_state['values']['bundle'];

  // Save.
  field_create_field($field_info);
  field_create_instance($field_instance);
  drupal_set_message('Field berhasil dibuat. Anda berada dihalaman edit field instance.');

  // Redirect to edit pages.
  $entity_get_info = entity_get_info();
  $url = $entity_get_info[$_entity_type]['bundles'][$_bundle]['admin']['real path'];
  $form_state['redirect'] = $url . '/fields/' . $_field_name;
}
