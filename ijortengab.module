<?php
/**
 * @file
 *   Module file.
 */

/**
 * Implements of hook_menu().
 */
function ijortengab_menu() {
  $items = array();
  // Coming soon.
  $items['admin/config/development/ijortengab'] = array(
    'title' => 'IjorTengab',
    'description' => 'IjorTengab pages. My Swiss Army Knife for "ngoprek" Drupal.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
    'expanded' => TRUE,
  );
  return $items;
}

/**
 * Fungsi ini digunakan pada hook_menu()
 * pada access callback jika dan hanya jika
 * pada page callback ber-value "system_admin_menu_block_page".
 *
 * Jika children kosong, maka menu akan disembunyikan oleh fungsi ini.
 */
function ijortengab_system_admin_menu_block_page_access($path) {
  if (user_is_admin()) {
    return TRUE;
  }

  // Kode dibawah ini hasil ngoprek fungsi system_admin_menu_block().
  $item = db_query("SELECT mlid, menu_name FROM {menu_links} ml WHERE ml.router_path = :path AND module = 'system'", array(':path' => $path))->fetchAssoc();
  $content = array();
  $query = db_select('menu_links', 'ml', array('fetch' => PDO::FETCH_ASSOC));
  $query->join('menu_router', 'm', 'm.path = ml.router_path');
  $query
    ->fields('ml')
    // Weight should be taken from {menu_links}, not {menu_router}.
    ->fields('m', array_diff(drupal_schema_fields_sql('menu_router'), array('weight')))
    ->condition('ml.plid', $item['mlid'])
    ->condition('ml.menu_name', $item['menu_name'])
    ->condition('ml.hidden', 0);

  foreach ($query->execute() as $link) {
    _menu_link_translate($link);
    if ($link['access']) {
      // The link description, either derived from 'description' in
      // hook_menu() or customized via menu module is used as title attribute.
      if (!empty($link['localized_options']['attributes']['title'])) {
        $link['description'] = $link['localized_options']['attributes']['title'];
        unset($link['localized_options']['attributes']['title']);
      }
      // Prepare for sorting as in function _menu_tree_check_access().
      // The weight is offset so it is always positive, with a uniform 5-digits.
      $key = (50000 + $link['weight']) . ' ' . drupal_strtolower($link['title']) . ' ' . $link['mlid'];
      $content[$key] = $link;
    }
  }
  // Tidak perlu sort.
  // ksort($content);
  // Jika content kosong, maka lebih baik item ini tidak perlu ditampilkan ke
  // user.
  // Menu administrators can see items for anonymous when administering.
  return !empty($content) || !empty($GLOBALS['menu_admin']);
}

/**
 * A default callback for all form that use ctools modal.
 * This code inspire from
 * https://www.deeson.co.uk/labs/insert-form-pop-modal-ctools-and-drupal-7
 */
function ijortengab_ctools_modal_form($js, $form_id) {
  if (!module_exists('ctools')) {
    return;
  }
  $form_state = array();

  $args = func_get_args();
  // Remove $js from the arguments.
  array_shift($args);
  // Remove $form_id from the arguments.
  array_shift($args);
  $form_state['build_info']['args'] = $args;
  $form_state['build_info']['ijortengab_ctools_modal_form'] = TRUE;
  if ($js) {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state ['ajax'] = TRUE;
    $form_state['form_alter'] = TRUE;
    $output = ctools_modal_form_wrapper($form_id, $form_state);
    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }
    print ajax_render($output);
    drupal_exit();
  }
  else {
    return drupal_build_form($form_id, $form_state);
  }
}

function ijortengab_ctools_modal_forma($js, $form_id) {
  // $debugname = 'js'; debug_ijor($$debugname, '$' . $debugname, 'f:__1');
  $form_state = array();
  ctools_include('ajax');
  ctools_include('modal');

  $args = func_get_args();
  // Remove $js from the arguments.
  array_shift($args);
  // Remove $form_id from the arguments.
  array_shift($args);
  $form_state['build_info']['args'] = $args;

  // Rebuild code from ctools_modal_form_wrapper().
  $form_state += array(
    're_render' => FALSE,
    'no_redirect' => $js,
    'ajax' => $js,
  );
  $form = drupal_build_form($form_id, $form_state);
  $form['#submit'][] = 'ijortengab_ctools_modal_form_submit';

  // Tambah submit form.
  if ($js) {
    $debugname = 'form'; dpm($$debugname, '$' . $debugname);
    if ((!$form_state['executed'] || $form_state['rebuild'])) {
      $output = ctools_modal_form_render($form_state, $form);
    }
    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }
    print ajax_render($output);
    drupal_exit();
  }
  return $form;
}

function ijortengab_form_alter(&$form, &$form_state) {
  if (!empty($form_state['build_info']['ijortengab_ctools_modal_form']) && !empty($form_state['ajax'])) {
    $form['#submit'][] = 'ijortengab_ctools_modal_form_submit';
  }
}

function ijortengab_ctools_modal_form_submit(&$form, &$form_state) {
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
}

/**
 * Implements of hook_element_info_alter().
 *
 * Digunakan untuk mendukung fitur form_scrolldown.
 * agar informasi #step dikenakan kepada semua subelement.
 */
function ijortengab_element_info_alter(&$type) {
  $type['radios']['#process'][] = 'ijortengab_process_step';
  $type['checkboxes']['#process'][] = 'ijortengab_process_step';
}

/**
 * Process element, copy property #step to child element.
 */
function ijortengab_process_step($element, &$form_state) {
  if (isset($element['#step'])) {
    foreach (element_children($element) as $sub) {
      $element[$sub]['#step'] = $element['#step'];
    }
  }
  return $element;
}


/**
 * Load user object.
 *
 * @param $user
 *   Flexible parameter. If the value's argument is NULL, it means current user
 *   (load from $GLOBALS['user']). The value's argument may as numeric (it means
 *   uid) or string (it means username) or user object.
 *
 * @return
 *   User object.
 */
function ijortengab_load_user($user = NULL) {
  $argument = $user;
  if (is_null($user)) {
    $argument = $GLOBALS['user']->uid;
  }
  if (is_object($user) && isset($user->uid)) {
    $argument = $user->uid;
  }

  static $info = array();
  if (!isset($info[$argument])) {
    $info[$argument] = FALSE;
    if (is_null($user)) {
      $info[$argument] = $GLOBALS['user'];
    }
    elseif (is_numeric($user)) {
      $info[$argument] = user_load($user);
    }
    elseif (is_string($user)) {
      $info[$argument] = user_load_by_name($user);
    }
    elseif (is_object($user) && isset($user->uid)) {
      $info[$argument] = $user;
    }
  }
  return $info[$argument];
}

// meng-attach javascript yang dibutuhkan untuk timeago.
// output html harus ada sbb:
// element table yang didalamnya terdapat
// element time dengan class timeago, dan attribute data-created-date
// attribute data-created-date harus berformat date(c);
function ijortengab_timeago() {
  $path = drupal_get_path('module', 'ijortengab');
  drupal_add_js($path . '/js/timeago/jquery.timeago.js');
  drupal_add_js($path . '/js/timeago/drupal.timeago.js');
  drupal_add_css($path . '/css/drupal.timeago.css');
  $settings = array(
    // 'refreshMillis' => (int) variable_get('timeago_js_refresh_millis', 60000),
    'allowFuture' => TRUE,
    // 'localeTitle' => (bool) variable_get('timeago_js_locale_title', 0),
    // 'cutoff' => (int) variable_get('timeago_js_cutoff', ''),
  );
  drupal_add_js(array('timeago' => $settings), 'setting');
}

/**
 *
 *
 */
function ijortengab_entity_metadata_wrapper_get($entity_type, $id, $field, $property = NULL) {
  if (!module_exists('entity')) {
    return;
  }

  $entity_wrapper = ijortengab_entity_metadata_wrapper($entity_type, $id);

  $entity_wrapper_info = $entity_wrapper->getPropertyInfo();

  if (isset($entity_wrapper_info[$field])) {
    $field_info = field_info_field($field);
    $value = $entity_wrapper->{$field}->value();
    // Field tipe file ternyata tidak mengembalikan object pada value,
    // melainkan array. Sebaiknya sih pake object sehingga sama dengan
    // hasil pada term, entity reference, maupun field collection.
    // Jadi.. perlu sedikit kita manipulasi.
    if (!is_null($value) && $field_info['type'] == 'file') {
      // Untuk multivalue.
      $is_multivalue = $field_info['cardinality'] > 1 || $field_info['cardinality'] == FIELD_CARDINALITY_UNLIMITED;
      if ($is_multivalue) {
        $value = array_map(function($each) { return (object) $each;}, $value);
      }
      else {
        $value = (object) $value;
      }
    }

    if (is_null($property)) {
      return $value;
    }

    if (is_array($value)) {
      $storage = array();
      foreach ($value as $object) {
        if (isset($object->{$property})) {
          $storage[] = $object->{$property};
        }
      }
      return $storage;
    }
    elseif (isset($value->{$property})) {
      return $value->{$property};
    }
  }
}

// fungsi set ini hanya berlaku jika parameter adalah numeric,
// karena object akan disimpan sebagai array dari static.
// bila parameter $id adalah object hasil dari entity_load,
// maka fungsi set hanya bisa berlaku pada satu baris untuk langsung di save.
// contoh:
// ijortengab_entity_metadata_wrapper_set()->save();
function ijortengab_entity_metadata_wrapper_set($entity_type, $id, $field) {
  if (!module_exists('entity')) {
    return;
  }
  $entity_wrapper = ijortengab_entity_metadata_wrapper($entity_type, $id);
  $entity_wrapper_info = $entity_wrapper->getPropertyInfo();
  if (isset($entity_wrapper_info[$field])) {
    $field_info = field_info_field($field);
    $is_multivalue = $field_info['cardinality'] > 1 || $field_info['cardinality'] == FIELD_CARDINALITY_UNLIMITED;
    // Get Value.
    $args = func_get_args();
    array_shift($args);
    array_shift($args);
    array_shift($args);
    // Untuk tipe file, kita membolehkan input berupa numeric, yang nanti kita
    // ubah menjadi object.
    if ($field_info['type'] == 'file') {
      $_ = function ($item) {
        $debugname = 'item'; dpm($$debugname, '$' . $debugname);
        if (is_numeric($item) && $load = file_load($item)) {
          return $load;
        }
        // Validasi jika input berupa array.
        if (is_array($item) && isset($item['fid']) && is_numeric($item['fid']) && $load = file_load($item)) {
          return $item;
        }
        // Selain itu, maka tidak valid, maka delete.
        return NULL;
      };
      // $debugname = 'field_info'; dpm($$debugname, '$' . $debugname);
      $args = array_map($_, $args);
    }
    if ($field == 'sirip_file') {
      $debugname = 'args'; dpm($$debugname, '$' . $debugname);
      $debugname = 'is_multivalue'; dvm($$debugname, '$' . $debugname);
      return;
    }

    // jika multivalue, maka perlu wrap sebagai array.
    if (!empty($args)) {
      $value = $is_multivalue ? $args : array_shift($args);
      $entity_wrapper->{$field}->set($value);
    }
  }
  return $entity_wrapper;
}

function ijortengab_entity_metadata_wrapper($entity_type, $id) {
  if (!module_exists('entity')) {
    return;
  }
  // Untuk penggunaan static, maka ubah $id dari load object ke numeric.
  if (is_object($id)) {
    $_id = entity_get_info($entity_type)['entity keys']['id'];
    $id = $id->{$_id};
  }
  // Kita mengharapkan bahwa $id adalah numeric. Jika ternyata tidak, maka
  // perlu di
  static $info = array();
  if (!isset($info[$entity_type][$id])) {
    $info[$entity_type][$id] = entity_metadata_wrapper($entity_type, $id);
  }
  return $info[$entity_type][$id];
}

/**
 * Mengembalikan nilai machine_name dari user's field.
 *
 * Berlaku pada Entity User yang memiliki field bertipe
 * entityreference, yang me-reference entity Taxonomy Term
 * dimana taxonomy term tersebut memiliki property
 * machine name. Machine name didapat dari module taxonomy_machine_name.
 * Bagan dapat dilukiskan sebagai berikut:
 * user -> field -> entityreference -> taxonomy_term -> machine_name.
 */
function ijortengab_user_field_value($field, $uid = NULL) {
  if (empty($uid)) {
    $uid = $GLOBALS['user']->uid;
  }
  $property = NULL;
  $info = field_info_field($field);
  if ($info['type'] == 'entityreference' && $info['settings']['target_type'] == 'taxonomy_term') {
    $property = 'machine_name';
  }
  return ijortengab_entity_metadata_wrapper_get('user', $uid, $field, $property);
}

/**
 * Mengembalikan nilai field dari node.
 *
 * Jika field tersebut bertipe entityreference yang mereference taxonomy_term
 * maka akan mengembalikan machine_name dari taxonomy tersebut alih-alih name.
 */
function ijortengab_node_field_value($field, $node_id, $default_property = TRUE) {
  $property = NULL;
  $info = field_info_field($field);
  if ($default_property) {
    switch ($info['type']) {
      case 'entityreference':
        switch ($info['settings']['target_type']) {
          case 'taxonomy_term':
            $property = 'machine_name';
            break 2;

          case 'user':
            $property = 'uid';
            break 2;

          case 'node':
            $property = 'nid';
            break 2;
        }
        break;

      case 'field_collection':
        $property = 'item_id';
        break;

      case 'file':
        $property = 'fid';
        break;
    }
  }
  return ijortengab_entity_metadata_wrapper_get('node', $node_id, $field, $property);
}

/**
 * Mengembalikan nilai machine_name dari taxonomy_term's field.
 *
 * Berlaku pada Entity Taxonomy Term yang memiliki field bertipe
 * entityreference, yang me-reference entity Taxonomy Term
 * dimana taxonomy term tersebut memiliki property
 * machine name. Machine name didapat dari module taxonomy_machine_name.
 * Bagan dapat dilukiskan sebagai berikut:
 * taxonomy_term -> field -> entityreference -> taxonomy_term -> machine_name.
 */
function ijortengab_taxonomy_term_attribute($field, $term_id) {
  return ijortengab_entity_metadata_wrapper_get('taxonomy_term', $term_id, $field, 'machine_name');
}

function ijortengab_is_sync_user_node_attribute($field, $nid, $uid = NULL) {
  $user = (array) ijortengab_user_field_value($field, $uid);
  $node = (array) ijortengab_node_field_value($field, $nid);
  return count(array_intersect($node, $user)) !== 0;
}

function ijortengab_is_user_set_as_reference($entity_type, $id, $field, $user = NULL) {
  if ($user = ijortengab_load_user($user)) {
    $list = (array) ijortengab_entity_metadata_wrapper_get($entity_type, $id, $field);
    if (!empty($list)) {
      // $list seharusnya adalah hasil dari load user, karena
      // ijortengab_entity_metadata_wrapper_get() menggunakan method value()
      // daripada method raw().
      $getuid = function ($obj) {
        return $obj->uid;
      };
      $uids = array_map($getuid, $list);
      return in_array($user->uid, $uids);
    }
  }
  return FALSE;
}

/**
 * Field yang berada di node dan user yang sama,
 * maka akan disesuaikan nilainya dengan user.
 */
function ijortengab_adjust_field_node_and_user($do, &$form, &$form_state) {
  // $debugname = 'form'; dpm($$debugname, '$' . $debugname);
  // cari field yang ada di user dan ada di node.
  $found = array();
  foreach(field_info_fields() as $field_name => $field) {
    if (isset($field['bundles']['node']) && isset($field['bundles']['user'])) {
      $found[$field_name] = $field;
    }
  }
  // $debugname = 'found'; dpm($$debugname, '$' . $debugname);
  foreach($found as $field_name => $field) {
    // Hanya support untuk column target_id dari module entity reference.
    if (isset($field['columns']['target_id'])) {
      $field_name = $field['field_name'];
      // Pastikan di form ada.
      if (isset($form[$field_name])) {
        // Saat ini hanya mendukung field yang terdapat language.
        if (isset($form[$field_name]['#language'])) {
          $lang = $form[$field_name]['#language'];
          if (isset($form[$field_name][$lang]['#options'])) {
            // sirip_submit_adjust_options_of_term_with_user_attribute($do, $form[$field_name][$lang], $field_name);
            $element = &$form[$field_name][$lang];
            $default_value = array_key_exists('#default_value', $element) ? $element['#default_value'] : array();
            //
            // $debugname = 'default_value'; dvm($$debugname, '$' . $debugname);
            $raw = entity_metadata_wrapper('user', $GLOBALS['user']->uid)->{$field_name}->raw();
            // $debugname = 'raw'; dvm($$debugname, '$' . $debugname);
            // Gabung semuanya.
            $diff = array_diff($raw, $default_value);
            $value = array_merge($default_value, $diff);
            $element['#default_value'] = $value;
            switch ($do) {
              case 'disabled':
                $element['#disabled'] = TRUE;
                break;
            }
          }
          switch ($do) {
            case 'hide':
              $form[$field_name]['#access'] = user_access('administer site configuration');
              break;
          }
        }
      }
    }
  }
}

/**
 * Memfilter pilihan term berkaitan dengan user saat ini.
 */
function ijortengab_adjust_options_term_and_user($do, $entity_field, $user_field, &$form, &$form_state) {
  // Pastikan di form ada.
  if (!isset($form[$entity_field])) {
    return;
  }
  // Saat ini hanya mendukung field yang terdapat language.
  if (isset($form[$entity_field]['#language'])) {
    $lang = $form[$entity_field]['#language'];
    if (isset($form[$entity_field][$lang]['#options'])) {
      _ijortengab_adjust_options_term_and_user($do, $form[$entity_field][$lang], $user_field);
    }
  }


}

function _ijortengab_adjust_options_term_and_user($do, &$element, $user_field) {
  $options = &$element['#options'];
  // $debugname = 'user_field'; dvm($$debugname, '$' . $debugname);
  // $debugname = 'options'; dpm($$debugname, '$' . $debugname);
  $user_value = ijortengab_user_field_value($user_field);
  // $debugname = 'user_value'; dpm($$debugname, '$' . $debugname);
  if (!empty($user_value)) {
    foreach ($options as $id => $label) {
      $term_value = ijortengab_taxonomy_term_attribute($user_field, $id);
      $remove = FALSE;
      if (is_array($term_value)) {
        $intersect = array_intersect($user_value, $term_value);
        if (empty($intersect)) {
          $remove = TRUE;
        }
      }
      elseif ($user_value !== $term_value) {
        $remove = TRUE;
      }
      if ($remove) {
        switch ($do) {
          case 'disabled':
            $element[$id]['#disabled'] = TRUE;
            $element[$id]['#weight'] = 1;
            break;

          case 'hide':
            unset($options[$id]);
            break;
        }
      }
    }
  }
}

/**
 * Mengcovert string dengan format .info
 *
 */
function ijortengab_toggle_info($input, $clean = FALSE) {
  if ($clean) {
    ijortengab_utf8_save($input);
  }
  $result = new parseINFO($input);
	return $result->result;
}

function ijortengab_machine_name_to_term_id($machine_name, $vocabulary) {
  if (module_exists('taxonomy_machine_name') && $load = taxonomy_term_machine_name_load($machine_name, $vocabulary)) {
    return $load->tid;
  }
}

/**
 * Clean dengan memapping semua karakter dengan utf_8.
 */
function ijortengab_utf8_save(&$input) {
  if (is_string($input)) {
    $input = utf8_encode($input);
  }
  elseif (is_array($input)) {
    foreach ($input as &$value) {
      ijortengab_utf8_save($value);
    }
  }
}

function ijortengab_modify_array($array_input, $array_added_or_key_to_moved, $key_pole, $mode = 'AFTER') {
  if (is_string($array_added_or_key_to_moved)) {
    $array_added[$array_added_or_key_to_moved] = $array_input[$array_added_or_key_to_moved];
    unset($array_input[$array_added_or_key_to_moved]);
  }
  elseif (is_array($array_added_or_key_to_moved))
    $array_added = $array_added_or_key_to_moved;
  else {
    return;
  }
  $array_keys = array_keys($array_input);
  $key = array_search($key_pole, $array_keys);
  $position = $key + 1;
  switch ($mode) {
    case 'AFTER':
      $offset = $position;
      break;

    case 'BEFORE':
      $offset = $position - 1;
      break;

  }
  $front = array_slice($array_input, 0, $offset);
  $back = array_slice($array_input, $offset);
  return $front + $array_added + $back;
}

/**
 * Depracated, use ijortengab_is_sync_user_node_attribute instead.
 */
function ijortengab_is_attribute_user_contains_attribute_node($field, $nid, $uid = NULL) {
  $user = (array)  ijortengab_user_field_value($field, $uid);
  $node = (array)  ijortengab_node_field_value($field, $nid);
  return count(array_intersect($node, $user)) !== 0;
}

function ijortengab_multiple_value(&$element, &$form_state, $callback) {
  $element += array(
    // '#parents' => array(),
    '#theme' => 'field_multiple_value_form',
    '#cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'add_more' => array(
      '#type' => 'submit',
      '#name' => 'cintakitabisagial',
      '#value' => 'tambah baris',
      '#ajax' => array(
        'callback' => 'field_add_more_js',
        'wrapper' => 'field_add_more_js',
        'effect' => 'fade',
      ),
    ),
  );
  if (is_callable($callback)) {
    $element[0] = $callback();
  }
}

/**
 * Fungsi mandiri, agak menyalahi aturan penamaan fungsi di Drupal,
 * dimana seharusnya setiap fungsi ada prefix nama module. Namun ini
 * sama kayak fungsi dpm-nya module devel, digunakan karena penggunaan yang
 * khusus.
 */
/**
 * Memerikasa apakah current user memiliki role sebagai Administrator.
 */
if (!function_exists('user_is_admin')) {
  function user_is_admin() {
    global $user;
    if($user->uid == 1) {
      return TRUE;
    }
    $rid = variable_get('user_admin_role');
    if (isset($rid)) {
      return isset($user->roles[$rid]);
    }
  }
}
/**
 * Depracated, gunakan user_is_admin().
 */
if (!function_exists('is_admin')) {
  function is_admin() {
    return user_is_admin();
  }
}
